package simulations

import io.gatling.core.Predef.{Simulation, _}
import scenarios.TotalScenario

import scala.concurrent.duration.DurationInt

class PersonalOffersMaxPerf extends Simulation{

  private val createUserExec = TotalScenario.createUserScenario
  setUp(createUserExec.inject(
    rampUsersPerSec(10) to 20 during (1 minutes),
    constantUsersPerSec(20) during (3 minutes),
    rampUsersPerSec(20) to 30 during (1 minutes),
    constantUsersPerSec(30) during (3 minutes),
  ))
    .assertions(
      global.responseTime.max.lt(1_000),
      global.successfulRequests.percent.gt(97)
    )
}
