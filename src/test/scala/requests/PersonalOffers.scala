package requests

import config.Config
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.request.builder.HttpRequestBuilder

object PersonalOffers {

  val get: HttpRequestBuilder = http("/customer/offers")
    .get(Config.app_url + "/customer/personal-offers?crmId=${crm_id}")
    .header("Accept","application/json")
    .disableFollowRedirect
    .check(status is 200)
}
