package scenarios

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import requests.PersonalOffers

object TotalScenario {

  private val feeder = csv("postgres_public_offers.csv").random

  var createUserScenario: ScenarioBuilder = scenario("Get personal offers by crmId")
    .feed(feeder)
    .exec(PersonalOffers.get)
}
